import os, datetime, logging

'''Time now'''
now = datetime.datetime.now().year


def remote_dir(path):
    for root, dirname, files in os.walk(path):
        if len(dirname) == 0 and len(files) == 0:
            os.rmdir(root + '/'.join(dirname))
            print('Delete : ' + root + '/'.join(dirname))

            '''logging'''
            logging.basicConfig(filename='Deletefile.log', format='%(asctime)s %(message)s')
            logging.warning("Delete directory: " + root + '/'.join(dirname))


def remote_file(path):
    for root, dirnames, files in os.walk(path):
        for i in files:

            '''path file to delete'''
            old_file = root + "/" + i

            '''time last modify'''
            timefile = datetime.datetime.fromtimestamp(os.path.getmtime(os.path.join(root + "/", i))).year
            try:
                if i.endswith('') and timefile < now - 5:
                    print(old_file)
                    print(timefile)

                    '''delete file'''
                    # os.remove(old_file)

                    '''logging'''
                    logging.basicConfig(filename='Deletefile.log', format='%(asctime)s %(message)s')
                    logging.warning('Deletefile: ' + old_file)

            except ValueError:
                print("Can't delete file: " + old_file)

                '''logging'''
                logging.basicConfig(filename='Deletefile.log', format='%(asctime)s %(message)s')
                logging.warning("Can't delete file: " + old_file)



'''call function remote_file input root path'''
remote_file(path='/home/nfs')
remote_dir(path='/home/nfs')